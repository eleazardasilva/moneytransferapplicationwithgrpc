<h1 align="center">  
  <br>
   Money transfer APP
  <br>
</h1>
<h4 align="center">An implementation of a money transaction application using <a href="https://grpc.io/" target="_blank">gRPC</a>.</h4>

<p align="center">
  <a href="#intro">Intro</a> •
  <a href="#project-features">Project Features</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#license">License</a> 
</p>

## Intro

This is a small project to showcase the use of gRPC in the implementation of server and client applications in java.
In this example, for simplicity the client and server are implemented in java, but gRPC is programming language agnostic:
<h1 align="center">
  <a href="https://grpc.io/"><img src="https://blog.jdriven.com/uploads/2018/10/grpc-overview.png" alt="gRPC" width="900"></a>
</h1>

Also for simplicity the client is implemented in the unit tests. 

## Project Features

* Server side application uses fixedThreadPool to handle multiple concurrent requests.
* The application uses the gRPC protocol buffer to define all objects (messages and entities).
* There is a persistence interface to define the interaction with the database.
* For simplicity the database is implemented as a simple class with lists.
* In this database I use java streams to demonstrate the implementation of a parallel collector.

## How To Use

Clone the project and first clean/compile to be able to view the objects that are created from the protocol buffer.
There is a test class with multiple tests that can validate the functionality.
To plug a different client, start the server on a port and user the proto-buffer to create requests in any supported language.
To clone and run this application, you'll need [Git](https://git-scm.com) and a java idea. 

From your command line:

```bash
# Clone this repository
$ git clone https://eleazardasilva@bitbucket.org/eleazardasilva/moneytransferapplicationwithgrpc.git

# Open the IDEA and open the project
# Two options to run the app:
## Run the unit tests
## Start the server and send messages with your own client.
```

## License

MIT

---