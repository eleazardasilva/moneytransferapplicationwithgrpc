package com.eleazar.backend.test;

import com.eleazar.backend.model.MoneyTransferApp.Currency;
import com.eleazar.backend.model.MoneyTransferApp.TransactionReply;
import com.eleazar.backend.model.MoneyTransferApp.TransferRequest;
import com.eleazar.backend.model.MoneyTransferApp.User;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Eleazar da Silva on 27-01-2019.
 * Simulates the interaction with a db
 */
public class InMemoryDataStore implements PersistenceInterface {

    /**
     * initial capacity added to improve performance.
     * Collections' capacity is changed dynamically when needed
     * By setting an approximate value we spare additional processing
     */
    private ArrayList<User> users = new ArrayList<>(15);
    private ArrayList<TransactionReply> transactions = new ArrayList<>(100);

    @Override
    public ArrayList<User> loadUsers() {

        User user1 = User.newBuilder().setName("Eleazar da Silva").setUserName("eleazarSilva")
                .putBalance(Currency.EUR_VALUE, "22.30")
                .build();
        User user2 = User.newBuilder().setName("Maria Afra").setUserName("mariaA")
                .putBalance(Currency.EUR_VALUE, "220.30")
                .build();
        User user3 = User.newBuilder().setName("Jon Silver").setUserName("jSilver")
                .putBalance(Currency.GBD_VALUE, "22.30")
                .build();
        User user4 = User.newBuilder().setName("Joana Maio").setUserName("Joana")
                .putBalance(Currency.USD_VALUE, "209.00")
                .putBalance(Currency.EUR_VALUE, "500.99")
                .build();
        User user5 = User.newBuilder().setName("Carlos").setUserName("carlos123")
                .putBalance(Currency.GBD_VALUE, "20.31")
                .build();
        User user6 = User.newBuilder().setName("Bruno Silva").setUserName("bruno")
                .putBalance(Currency.EUR_VALUE, "22.30")
                .build();
        User user7 = User.newBuilder().setName("Maria Silva").setUserName("mariaS")
                .putBalance(Currency.EUR_VALUE, "220.30")
                .build();
        User user8 = User.newBuilder().setName("Jon Smith").setUserName("jSmith")
                .putBalance(Currency.GBD_VALUE, "22.30")
                .build();
        User user9 = User.newBuilder().setName("Mary Cequeira").setUserName("MaryC")
                .putBalance(Currency.USD_VALUE, "209.00")
                .putBalance(Currency.EUR_VALUE, "500.99")
                .build();
        User user10 = User.newBuilder().setName("Carl").setUserName("carl10")
                .putBalance(Currency.GBD_VALUE, "20.30")
                .build();

        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);
        users.add(user10);

        return users;
    }

    @Override
    public ArrayList<TransactionReply> loadTransactions() {
        return transactions;
    }

    @Override
    public void addUser(String name, String userName) {
        User user = User.newBuilder().setName(name).setUserName(userName)
                .build();
        users.add(user);
    }

    @Override
    public void addTransaction(TransactionReply transaction) {
        transactions.add(transaction);
    }

    @Override
    public User getUser(String userName) {
        return users.stream().parallel().filter(a -> a.getUserName().equals(userName)).distinct().findFirst().get();
    }

    public boolean validateUser(String userName) {
        return users.stream().parallel().filter(a -> a.getUserName().equals(userName)).distinct().count() > 0;
    }

    @Override
    public TransactionReply getTransaction(int transactionId) {
        return transactions.stream().parallel().filter(a -> a.getRequest().getTransactionId() == transactionId).distinct().findFirst().get();
    }

    @Override
    public BigDecimal getUserBalance(String userIn, Currency currency) {
        User user = users.stream().parallel().filter(a -> a.getUserName().equals(userIn)).distinct().findFirst().get();
        // validates if balance exists in asked currency
        return new BigDecimal(user.getBalanceOrDefault(currency.getNumber(),"0.00").trim());
    }

    @Override
    public boolean creditAccount(String userTo, BigDecimal amount, Currency currency) {
        User user = users.stream().parallel().filter(a -> a.getUserName().equals(userTo)).distinct()
                .findFirst().get();
        // recalculate balance.
        BigDecimal newBalance = user.containsBalance(currency.getNumber()) ?
                new BigDecimal(user.toBuilder().getBalanceMap().get(currency.getNumber())).add(amount)
                :amount;

        // sets user with new balance
        User newUser = user.toBuilder().putBalance(currency.getNumber(),
                newBalance.toString()).build();
        users.replaceAll(a -> {
            if (a.getUserName().equals(newUser.getUserName())) return newUser;
            return a;
        });

        // missing commit validation
        return true;
    }

    @Override
    public boolean debitAccount(String userFrom, BigDecimal amount, Currency currency) {
        User user = users.stream().parallel().filter(a -> a.getUserName().equals(userFrom)).distinct()
                .findFirst().get();
        // recalculate balance.
        BigDecimal newBalance = new BigDecimal(user.getBalanceMap().get(currency.getNumber())).subtract(amount);

        // sets the user with the new balance
        User newUser = user.toBuilder().putBalance(currency.getNumber(),
                newBalance.toString())
                .build();
        users.replaceAll(a -> {
            if (a.getUserName().equals(newUser.getUserName())) return newUser;
                return a;
        });

        // missing commit validation
        return true;
    }

    @Override
    public ArrayList<TransferRequest> getUserTransactions(String userName) {
        return transactions.stream().parallel()
                .collect(() -> new TransactionCollector(userName), TransactionCollector::accumulate, TransactionCollector::combine)
                .getResult();
    }
}

// customised collector class
class TransactionCollector {

    private ArrayList<TransferRequest> transactions = new ArrayList<>();
    private String userName;

    TransactionCollector(String userName) {
        this.userName = userName;
    }

    ArrayList<TransferRequest> getResult() {
        return transactions;
    }

    void accumulate(TransactionReply transfer) {
        // Successful transactions of the user
        if (transfer.getRequest().getUserNameFrom().equals(userName) && transfer.getReply().getReplyCode() == 0) {
            transactions.add(transfer.getRequest());
        }
    }

    void combine(TransactionCollector other) {
        transactions.addAll(other.transactions);
    }
}