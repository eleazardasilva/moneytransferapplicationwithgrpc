package com.eleazar.backend.test;

import com.eleazar.backend.model.MoneyTransferApp.*;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Eleazar da Silva on 26-01-2019.
 *
 */
class TransactionProcessor {

    private InMemoryDataStore dataStore;

    TransactionProcessor(){
        dataStore = new InMemoryDataStore();
        dataStore.loadUsers();
    }

    synchronized GetBalanceReply getBalance(GetBalanceRequest request) {
        GetBalanceReply balanceReply;
        String userName = request.getUserName();
        boolean userValid = isValidUser(userName);
        BigDecimal balance;

        if (userValid) {
            balance = dataStore.getUserBalance(request.getUserName(), request.getCurrency());
        } else {
            balanceReply = GetBalanceReply.newBuilder()
                    .setReplyCode(1).setReplyDescription("User invalid").build();
            return balanceReply;
        }

        balanceReply = GetBalanceReply.newBuilder()
                .setReplyCode(0).setReplyDescription("Success").setBalance(balance.toString()).build();

        return balanceReply;
    }

    synchronized ListOfTransactionsReply getListOfTransactions(ListOfTransactionsRequest request) {
        ArrayList<TransferRequest> transactions = dataStore.getUserTransactions(request.getUserName());
        return ListOfTransactionsReply.newBuilder().setReplyCode(0).setReplyDescription("Success")
                .addAllTransactions(transactions).build();
    }

    // handles money transference
    synchronized TransferReply processTransference(TransferRequest request) {
        String userFrom = request.getUserNameFrom();
        String userTo = request.getUserNameTo();
        BigDecimal amount = new BigDecimal(request.getTransactionAmount());
        Currency currency = request.getCurrency();
        boolean debitDone;
        boolean creditDone;
        TransferReply reply;

        // validate users and balance and attempts to processTransference the transaction
        if (!isValidUser(userFrom)) {
            reply = TransferReply.newBuilder()
                    .setReplyCode(3)
                    .setReplyDescription("Debit user not found")
                    .build();
        } else if (!isValidUser(userTo)) {
            reply = TransferReply.newBuilder()
                    .setReplyCode(4)
                    .setReplyDescription("Credit user not found")
                    .build();
        } else if (isBalanceAvailable(userFrom, amount, currency)) {
            debitDone = isDebitAccount(userFrom, amount, currency);
            creditDone = isCreditAccount(userTo, amount, currency);

            if (debitDone && creditDone) {
                reply = TransferReply.newBuilder()
                        .setReplyCode(0)
                        .setReplyDescription("Success")
                        .build();
            } else if (!debitDone) {
                reply = TransferReply.newBuilder()
                        .setReplyCode(1)
                        .setReplyDescription("Not able to debit user")
                        .build();
            } else {
                reply = TransferReply.newBuilder()
                        .setReplyCode(2)
                        .setReplyDescription("Not able to credit user")
                        .build();
            }
        } else {
            reply = TransferReply.newBuilder()
                    .setReplyCode(3)
                    .setReplyDescription("Balance unavailable for the transaction")
                    .build();
        }

        registerTransaction(request, reply);
        return reply;
    }

    synchronized TransactionReply getTransaction(TransactionRequest request) {

        TransactionReply transaction = dataStore.getTransaction(request.getTransactionId());

        return TransactionReply.newBuilder()
                .setReplyCode(0).setReplyDescription("Success")
                .setRequest(transaction.getRequest()).setReply(transaction.getReply()).build();
    }

    private void registerTransaction(TransferRequest request, TransferReply reply) {
        TransactionReply transaction = TransactionReply.newBuilder().setRequest(request).setReply(reply).build();
        dataStore.addTransaction(transaction);
    }

    private boolean isDebitAccount(String userFrom, BigDecimal amount, Currency currency) {
        return dataStore.debitAccount(userFrom, amount, currency);
    }

    private boolean isCreditAccount(String userTo, BigDecimal amount, Currency currency) {
        return dataStore.creditAccount(userTo, amount, currency);
    }

    private boolean isValidUser(String userIn) {
        return dataStore.validateUser(userIn);
    }

    private boolean isBalanceAvailable(String userFrom, BigDecimal amount, Currency currency) {

        BigDecimal balance = dataStore.getUserBalance(userFrom, currency);
        return (balance.subtract(amount)).compareTo(new BigDecimal(0.00)) > 0;
    }
}
