package com.eleazar.backend.test;

import com.eleazar.backend.model.MoneyTransferApp.*;
import com.eleazar.backend.model.TransactionServiceGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Eleazar da Silva on 24-01-2019.
 * Default gRPC server definition.
 *
 */

public class TransactionServiceServer {
    public static void main(String[] args) throws IOException {
        // custom fixed executor with number of threads
        ExecutorService executorService = Executors.newFixedThreadPool(Integer.parseInt(args[1]));
        // server configured to listen on defined port for the service requests
        Server server = ServerBuilder.forPort(Integer.parseInt(args[0]))
                .addService(new TransactionServiceServerImpl())
                .executor(executorService)
                .build();
        server.start();
        try {
            // to keep the server running
            server.awaitTermination();
            executorService.shutdown();
        } catch (InterruptedException e) {
            System.out.println("Server killed");
        }


    }
}

class TransactionServiceServerImpl extends TransactionServiceGrpc.TransactionServiceImplBase {

    private TransactionProcessor transactionProcessor;

    // this constructor is only important to load the transaction processor
    TransactionServiceServerImpl() {
        super();
        transactionProcessor = new TransactionProcessor();
    }

    @Override
    public void transferFunds(TransferRequest request, StreamObserver<TransferReply> responseObserver) {

        try {
            responseObserver.onNext(transactionProcessor.processTransference(request));
            responseObserver.onCompleted();
        } catch (Exception e) {
            TransferReply reply = TransferReply.newBuilder()
                    .setReplyCode(999)
                    .setReplyDescription("Transfer funds error: " + e.toString())
                    .build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

    }

    @Override
    public void transferFundsReversal(ReversalRequest request, StreamObserver<ReversalReply> responseObserver) {
        super.transferFundsReversal(request, responseObserver);
    }

    @Override
    public void getTransaction(TransactionRequest request, StreamObserver<TransactionReply> responseObserver) {
        try {
            responseObserver.onNext(transactionProcessor.getTransaction(request));
            responseObserver.onCompleted();
        } catch (Exception e) {
            TransactionReply reply = TransactionReply.newBuilder()
                    .setReplyCode(999)
                    .setReplyDescription(e.toString())
                    .build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getClientBalance(GetBalanceRequest request, StreamObserver<GetBalanceReply> responseObserver) {
        try {
            responseObserver.onNext(transactionProcessor.getBalance(request));
            responseObserver.onCompleted();
        } catch (Exception e) {
            GetBalanceReply reply = GetBalanceReply.newBuilder()
                    .setReplyCode(999)
                    .setReplyDescription(e.toString())
                    .build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getUserTransactions(ListOfTransactionsRequest request, StreamObserver<ListOfTransactionsReply> responseObserver) {
        try {
            responseObserver.onNext(transactionProcessor.getListOfTransactions(request));
            responseObserver.onCompleted();
        } catch (Exception e) {
            ListOfTransactionsReply reply = ListOfTransactionsReply.newBuilder()
                    .setReplyCode(999)
                    .setReplyDescription(e.toString())
                    .build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }
    }

    @Override
    public void testConnectivity(testRequest request, StreamObserver<testReply> responseObserver) {
        try {
            responseObserver.onNext(testReply.newBuilder().setResponseCode(0).build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onNext(testReply.newBuilder().setResponseCode(999).build());
            responseObserver.onCompleted();
        }
    }
}
