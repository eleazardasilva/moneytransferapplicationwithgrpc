package com.eleazar.backend.test;

import com.eleazar.backend.model.MoneyTransferApp.Currency;
import com.eleazar.backend.model.MoneyTransferApp.TransactionReply;
import com.eleazar.backend.model.MoneyTransferApp.TransferRequest;
import com.eleazar.backend.model.MoneyTransferApp.User;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Eleazar da Silva on 27-01-2019.
 * Defines the data persistence contract
 */
public interface PersistenceInterface {

    ArrayList<User> loadUsers();

    ArrayList<TransactionReply> loadTransactions();

    void addUser(String name, String userName);

    void addTransaction(TransactionReply transaction);

    boolean validateUser(String userName);

    User getUser(String userName);

    TransactionReply getTransaction(int transactionId);

    BigDecimal getUserBalance(String userIn, Currency currency);

    boolean debitAccount(String userFrom, BigDecimal amount, Currency currency);

    boolean creditAccount(String userTo, BigDecimal amount, Currency currency);

    ArrayList<TransferRequest> getUserTransactions(String userName);
}
