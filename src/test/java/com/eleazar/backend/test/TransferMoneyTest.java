package com.eleazar.backend.test;

import com.eleazar.backend.model.MoneyTransferApp.*;
import com.eleazar.backend.model.TransactionServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.junit.*;
import org.junit.runners.MethodSorters;
import resources.MyJUnitStopWatch;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by Eleazar da Silva on 24-01-2019.
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransferMoneyTest {

    private static TransactionServiceGrpc.TransactionServiceBlockingStub stub;
    private static Thread serverThread;
    private Date date = new Date();
    private long time = date.getTime();
    private Timestamp ts = new Timestamp(time);

    @Rule
    public MyJUnitStopWatch stopwatch = new MyJUnitStopWatch();

    @BeforeClass
    public static void doOneTimeSetup()
    {
        // Create the server on a separated thread to simulate client-server communication
        Runnable serverTask = () -> {
            try{
                String port = "50200";
                String threads = "7";
                // Arguments to start server: port & number of threads
                TransactionServiceServer.main(new String[] {port, threads});
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        serverThread = new Thread(serverTask);

        serverThread.start();

        // create client
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:50200")
                .usePlaintext()
                .build();

        stub = TransactionServiceGrpc.newBlockingStub(channel);
    }

    @AfterClass
    public static void doOneTimeTeardown() {
        if (serverThread.isAlive()) {
            serverThread.interrupt();
        }
    }

    @Test
    public void test00ServerConnection(){
        // this dummy users need to be in the database
        testReply testReply = stub.testConnectivity(testRequest.newBuilder().setDummyMessage("test connection").build());
        assertEquals("Test Transaction ErrorCode","0", String.valueOf(testReply.getResponseCode()));
    }

    @Test
    public void test01SuccessfulTransference(){
        // this dummy users need to be in the database
        TransferReply transferReply = stub.transferFunds(TransferRequest.newBuilder()
                .setUserNameFrom("Joana").setUserNameTo("eleazarSilva")
                .setCurrency(Currency.EUR).setTransactionAmount("30.99")
                .setTransactionTimestamp(ts.toString()).setTransactionId(101).build());

        assertEquals("Transaction ErrorCode","0 - Success",
                transferReply.getReplyCode() + " - " + transferReply.getReplyDescription());
    }

    @Test
    public void test02BalanceCredited() {
        // this dummy users need to be in the database
        TransferReply transferReply = stub.transferFunds(TransferRequest.newBuilder()
                .setUserNameFrom("Joana").setUserNameTo("eleazarSilva")
                .setCurrency(Currency.EUR).setTransactionAmount("30.99")
                .setTransactionTimestamp(ts.toString()).setTransactionId(102).build());

        assertEquals("Transaction ErrorCode","0 - Success",
                transferReply.getReplyCode() + " - " + transferReply.getReplyDescription());

        GetBalanceReply balanceReply = stub.getClientBalance(GetBalanceRequest.newBuilder()
                .setUserName("Joana").setCurrency(Currency.EUR).build());

        assertEquals("Balance ErrorCode", "0 - Success",
                balanceReply.getReplyCode() + " - "+ balanceReply.getReplyDescription());
        assertEquals("Balance","439.01", String.valueOf(balanceReply.getBalance()));
    }

    @Test
    public void test03UnsuccessfulTransaction() {
        // this dummy users need to be in the database
        TransferReply transferReply = stub.transferFunds(TransferRequest.newBuilder()
                .setUserNameFrom("Joana").setUserNameTo("eleazarSilva")
                .setCurrency(Currency.EUR).setTransactionAmount("3000.99")
                .setTransactionTimestamp(ts.toString()).setTransactionId(103).build());

        assertEquals("Transaction ErrorCode","3 - Balance unavailable for the transaction",
                transferReply.getReplyCode() + " - " + transferReply.getReplyDescription());
    }

    @Test
    public void test04CreditUserWithNewCurrency() {
        GetBalanceReply balanceReplyBefore = stub.getClientBalance(GetBalanceRequest.newBuilder()
                .setUserName("eleazarSilva").setCurrency(Currency.GBD).build());

        // this dummy users need to be in the database
        TransferReply transferReply = stub.transferFunds(TransferRequest.newBuilder()
                .setUserNameFrom("carlos123").setUserNameTo("eleazarSilva")
                .setCurrency(Currency.GBD).setTransactionAmount("10.99")
                .setTransactionTimestamp(ts.toString()).setTransactionId(104).build());

        assertEquals("Transaction ErrorCode","0 - Success",
                transferReply.getReplyCode() + " - " + transferReply.getReplyDescription());

        GetBalanceReply balanceReplyAfter = stub.getClientBalance(GetBalanceRequest.newBuilder()
                .setUserName("eleazarSilva").setCurrency(Currency.GBD).build());

        assertEquals("Balance Before and after", "0.00 - 10.99", balanceReplyBefore.getBalance()
                + " - " + balanceReplyAfter.getBalance());
    }

    @Test
    public void test05GetTransactionInfo() {
        TransactionReply transaction = stub.getTransaction(TransactionRequest.newBuilder()
                .setTransactionId(101).build());

        assertEquals("Transaction From", "Joana", transaction.getRequest().getUserNameFrom());
        assertEquals("Transaction To", "eleazarSilva", transaction.getRequest().getUserNameTo());
        assertEquals("Transaction Amount", "30.99", String.valueOf(transaction.getRequest().getTransactionAmount()));
        assertEquals("Transaction Result", 0, transaction.getReply().getReplyCode());
        System.out.println(transaction);
    }

    @Test
    public void test06ListUserTransactions() {
        ListOfTransactionsReply listOfTransactionsReply = stub.getUserTransactions(ListOfTransactionsRequest
                .newBuilder().setUserName("Joana").build());

        System.out.println(listOfTransactionsReply);

        assertEquals("Number of Transactions", 2, listOfTransactionsReply.getTransactionsCount());
    }

    /*
        Multithreading test exposes concurrency problem with current data store implementation
        The following exception will randomly be caught:
        replyCode: 999
        replyDescription: "java.util.ConcurrentModificationException"
     */
    @Test
    public void test07MultipleRequest() throws InterruptedException {
        Runnable requestTask = () -> {
            GetBalanceReply balanceReplyBefore = stub.getClientBalance(GetBalanceRequest.newBuilder()
                    .setUserName("eleazarSilva").setCurrency(Currency.GBD).build());
            String amountCreditedBefore = balanceReplyBefore.getBalance();
            System.out.println("Balance before: " + amountCreditedBefore);
            // this dummy users need to be in the database
            TransferReply transferReply = stub.transferFunds(TransferRequest.newBuilder()
                    .setUserNameFrom("carlos123").setUserNameTo("eleazarSilva")
                    .setCurrency(Currency.GBD).setTransactionAmount("0.9")
                    .setTransactionTimestamp(ts.toString()).setTransactionId(104).build());

            System.out.println(transferReply);

            GetBalanceReply balanceReplyAfter = stub.getClientBalance(GetBalanceRequest.newBuilder()
                    .setUserName("eleazarSilva").setCurrency(Currency.GBD).build());
            String amountCreditAfter = balanceReplyAfter.getBalance();
            System.out.println("Balance After: " + amountCreditAfter);

            GetBalanceReply balanceDebitedReplyAfter = stub.getClientBalance(GetBalanceRequest.newBuilder()
                    .setUserName("carlos123").setCurrency(Currency.GBD).build());
            String amountDebited = balanceDebitedReplyAfter.getBalance();
            System.out.println("Balance debited After: " + amountDebited);
        };

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        for (int i = 0; i <= 20; i++) {
            executorService.submit(requestTask);
        }
        executorService.awaitTermination(2, TimeUnit.SECONDS);
        GetBalanceReply balanceReplyAfter = stub.getClientBalance(GetBalanceRequest.newBuilder()
                .setUserName("eleazarSilva").setCurrency(Currency.GBD).build());
        String amountCreditAfter = balanceReplyAfter.getBalance();
        System.out.println("Last Balance After thread pool: " + amountCreditAfter);
    }
}
